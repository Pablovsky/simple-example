import { runInNewContext } from "vm";

export default {
    data(){
        return {
            verifyLogged: null
        };
    },
    methods:{
        verify(){
            this.newVerify();
            this.verifyLogged = setInterval(() => {
                this.newVerify();
            }, 5000);
        },
        newVerify(){
            if(typeof localStorage.logged === 'undefined'){
                //alert('redireccion');
                this.$router.push({name:'login'});
            }else if(localStorage.logged !="true"){
                this.$router.push({name:'login'});
                next();
            }
            
        }
    },
    beforeDestroy () {
        //console.log('adasd');
        clearInterval(this.verifyLogged)
    },

}