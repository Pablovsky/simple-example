import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: require('./views/home').default
    },
    {
      path: '/todo',
      name: 'Lista ToDo',
      component: require('./views/todo').default
    },
    {
      path: '/login',
      name: 'login',
      component: require('./views/login').default
    },
    {
      path: '/register',
      name: 'register',
      component: require('./views/register').default
    },
    {
      path: '/product',
      name: 'product',
      component: require('./views/product').default
    },





    {
      path: '*',
      component: require('./views/404').default
    },
  ],
  mode: 'history'
});
